import calculator.*;
import java.util.Scanner;
import java.lang.*;
public class calculatorproject {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("1. To open Standard Calculator \n2. To open Scientific Calculator \n3. For Expression");
        char mode = in.next().charAt(0);
        double result;
        if (mode == '1') {
            System.out.println("enter the operator");
            String ch = in.next();
            if (ch.equals("+") || ch.equals("-") || ch.equals("*") || ch.equals("/") || ch.equals("%") || ch.equals("pow") || ch.equals("root") || ch.equals("c")) {
                System.out.println("Enter the values for operation: ");
                Double a = in.nextDouble();
                Double b = in.nextDouble();
                stdcalc std = new stdcalc();
                result = std.evaluate(a, b, ch);
                System.out.println("Standard Operation: " + result);
                System.out.println("“M”:remember the result of the last valid operation.\n“MR” :print the contents of the memory in the next line.\n“MC” :clear the contents of the memory.\n");
                Scanner Input = new Scanner(System.in);
                String mem = Input.next();
                if (mem.equals("M")) {
                    std.setMemory(result);
                    System.out.println("Result Saved in memory.");
                } else if (mem.equals("MR")) {
                    System.out.println(std.getMemory());
                } else if (mem.equals("MC")) {
                    std.setMemory(0);
                    System.out.println("Memory Cleared.");
                } else {
                    System.out.println("Invalid Operation: " + ch);
                } } } else if (mode == '2') {
            System.out.println("enter the operator");
            String ch = in.next();
            if (ch.equals("sin") || ch.equals("cos") || ch.equals("tan") || ch.equals("log") || ch.equals("c")) {
                System.out.println("Enter the value for operation: ");
                Double a = in.nextDouble();
                scicalc sci = new scicalc();
                result = sci.evaluate(a, ch);
                sci.setMemory(result);
                System.out.println("Scientific Operation: " + result);
                System.out.println("“M” : should remember the result of the last valid operation.\n“MR” : should print the contents of the memory in the next line.\n“MC” : should clear the contents of the memory.\n");
                Scanner Input = new Scanner(System.in);
                String mem = Input.next();
                if (mem.equals("M")) {
                    sci.setMemory(result);
                    System.out.println("Result Saved in memory.");
                } else if (mem.equals("MR")) {
                    System.out.println(sci.getMemory());
                } else if (mem.equals("MC")) {
                    sci.setMemory(0);
                    System.out.println("Memory Cleared.");
                } else {
                    System.out.println("Invalid Operation: " + ch);
                } } }else {
                System.out.println("enter expression");
                Scanner input1 = new Scanner(System.in);
                String exp = input1.nextLine();
                double n = calc.evaluate(exp);
                System.out.println("Result: " + n);
            }}}



